import java.util.Scanner;

public class round {
    public static String[] time = { "10.00", "12.00", "14.00" };
    public static String[] date = { "20/08/66", "21/08/66", "22/08/66" };

    public static void Dates(String[] User) {
        Scanner kb = new Scanner(System.in);
        System.out.println("* Date *");
        for (int i = 0; i < date.length; i++) {
            System.out.println((i + 1) + "." + date[i] + " ");

        }
        System.out.println();
        SetDates(User);
    }

    public static boolean SetDates(String[] User) {
        Scanner kb = new Scanner(System.in);
        String SelectDates = kb.next();
        int n;
        while (true) {
            for (int i = 0; i < User.length; i++) {
                if (SelectDates.matches("[1-3]")) {
                    n = Integer.parseInt(SelectDates);
                    User[1] = date[i];
                    return true;

                } else {
                    System.out.println("* Please select again. *");
                    Dates(User);
                    return false;
                }
            }
        }

    }

    public static void times(String[] User) {
        Scanner kb = new Scanner(System.in);
        System.out.println("* Time *");
        for (int i = 0; i < time.length; i++) {
            System.out.println((i + 1) + "." + time[i] + " ");

        }
        System.out.println();
        settimes(User);
    }

    public static boolean settimes(String[] User) {
        Scanner kb = new Scanner(System.in);
        String m = kb.next();
        int n;
        while (true) {
            for (int i = 0; i < time.length; i++) {
                if (m.matches("[1-3]")) {
                    n = Integer.parseInt(m);
                    User[2] = time[i];
                    return true;

                } else {
                    System.out.println("* Please select again. *");
                    times(User);
                    return false;
                }
            }
        }

    }
}
